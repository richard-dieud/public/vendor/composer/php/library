<?php

namespace Rd\Vendor\Php\Model;

abstract class Something
{
    /**
     * @var int
     */
    protected $ID = null;
    /**
     * @var string
     */
    protected $created_at = null;
    /**
     * @var string
     */
    protected $updated_at = null;

    public function getID()
    {
        return $this->ID;
    }
    public function setID($value)
    {
        $this->ID = $value;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
    }

    protected function _getProps($thiz, $skip_id = false)
    {
        $props = get_object_vars($thiz);
        if ($skip_id) {
            unset($props['ID']);
        }

        return $props;
    }

    abstract public function getProps();

    abstract protected  function getTableName();
}
