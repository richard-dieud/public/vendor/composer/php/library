<?php

namespace Rd\Vendor\Php\Utils;

class ArrayLib
{
    public static function array_every($arr, $callback)
    {
        // is_callable is suitable for strings; when they orher is for anon function
        if (!empty($arr) && is_array($arr) && isset($callback) && (is_callable($callback) || $callback instanceof \Closure)) {

            $found =  array_map($callback, $arr);

            if (!empty($found) && is_array($found)) {
                return array_reduce($found, function ($a, $b) {
                    return $a && $b;
                }, current($found));
            }
        }
    }

    public static function implode_assoc($arr, $separator1 = '=', $separator2 = ' ')
    {
        return array_reduce(array_keys($arr), function ($a, $b) use ($arr, $separator1, $separator2) {
            $element = "$b$separator1'$arr[$b]'";
            $element = empty($a) ? "$element" : $a . "$separator2$element";
            return $element;
        });
    }
}
